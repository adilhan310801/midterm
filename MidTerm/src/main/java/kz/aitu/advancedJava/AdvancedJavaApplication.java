package kz.aitu.advancedJava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan
@SpringBootApplication
public class AdvancedJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdvancedJavaApplication.class, args);
	}

}
