package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Person;
import kz.aitu.advancedJava.service.PersonService;
import kz.aitu.advancedJava.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class PersonController {
    @Autowired
    private final PersonService personService;
    @Autowired
    private PersonRepository personRepository;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/api/persons/{personId}")
    public ResponseEntity<?> getRecord(@PathVariable Long personId) {
        return ResponseEntity.ok(personService.getById(personId));
    }


    @GetMapping("/api/v2/users")
    public String showTables(Model model) {
        Iterable<Person> item = personRepository.findAll();
        model.addAttribute("item", item);
        return "index";
    }

    @PostMapping("/api/v2/users")
    public String editTables(Model model) {
        Iterable<Person> item = personRepository.findAll();
        model.addAttribute("item", item);
        return "index";
    }
    @DeleteMapping("/api/v2/users/{personId}")
    public String deleteTables(Model model) {
        Iterable<Person> item = personRepository.findAll();
        model.addAttribute("item", item);
        return "index";
    }

    @PostMapping("/api/persons")
    public ResponseEntity<?> saveRecord(@RequestBody Person person) {
        return ResponseEntity.ok(personService.create(person));
    }

    @PutMapping("/api/persons")
    public ResponseEntity<?> updateRecord(@RequestBody Person person) {
        return ResponseEntity.ok(personService.create(person));
    }

    @DeleteMapping("/api/persons/{personId}")
    public void deleteRecord(@PathVariable Long personId) {
        personService.delete(personId);
    }
}
