CREATE TABLE Person (
                        id serial PRIMARY KEY NOT NULL,
                        firstname varchar(255) NOT NULL,
                        lastname varchar(255) NOT NULL ,
                        city varchar(255) NOT NULL,
                        phone varchar(255) NOT NULL,
                        telegram varchar(255) NOT NULL
);
